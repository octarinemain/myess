$(document).ready(function() {

    // Nav
    $('.nav-drop').hover(dropMenu, dropMenu);

    function dropMenu() {
        $(this).toggleClass('active');
    }

    // Copy
    $('.copy.single').on('click', function() {
        $('.copy-link.single').select();
        document.execCommand('copy');
    });

    // Copy Table
    $('.copy.tb').on('click', function() {
        $(this).closest('.tab-line').find('.copy-link.tb').select();
        document.execCommand('copy');
    });

    // Copy Card
    $('.copy.card').on('click', function() {
        $(this).closest('.files-card').find('.copy-link.card').select();
        document.execCommand('copy');
    });

    // Exit Cookies
    $('.cookies .exit').on('click', function() {
        $('.cookies').hide('slow');
    });

    // Popup
    $('#download').on('click', function(e) {
        e.preventDefault();
        setTimeout(function() {
            $('.popup').addClass('active');
        }, 5000);

        $('.loader').addClass('active');

        $('.popup').on('click', function(e) {
            var div = $(".pop-continue");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('active');
            }
        });
    });

    $('.exit-pop').on('click', function() {
        $('.popup').removeClass('active');
    });

    $('.burger').on('click', function() {
        $('.nav-wrap').addClass('active');
    });

    $('.ex-mob').on('click', function() {
        $('.nav-wrap').removeClass('active');
    });

    $('.exit-notif').on('click', function() {
        $('.notif').removeClass('active');
    });

    $('#pop-up').on('click', function() {
        $('.popup').addClass('active');

        $('.popup').on('click', function(e) {
            var div = $(".pop-continue");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('active');
            }
        });
    });



    // Speed
    if ($('div').hasClass('speed')) {
        setInterval(function() {
            $('.speed').toggleClass('active');
        }, 100);
    }

    // Sound
    if ($('div').hasClass('sound-screen')) {
        setInterval(function() {
            $('.sound-screen').toggleClass('active');
        }, 200);
    }

    // Accordion
    if ($('div').hasClass('accordion')) {
        $('.accordion').accordion({
            "transitionSpeed": 400
        });
    }

    // Scroll fullpage
    function scrollFullPage() {
        var scrollPage = new fullpage('#fullpage', {
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            anchors: ['1stPage', '2ndPage', '3rdPage', '4thPage', '5thPage', '6thPage', '7thPage', '8thPage', '9thPage', '10thPage'],
        });
    };
    var isFullPage = $('*').is('#fullpage');

    if (isFullPage) {
        scrollFullPage();
    }

});